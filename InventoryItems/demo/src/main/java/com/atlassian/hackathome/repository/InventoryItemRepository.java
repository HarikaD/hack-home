package com.atlassian.hackathome.repository;

import com.atlassian.hackathome.model.InventoryItemDetails;
import org.springframework.data.repository.CrudRepository;

public interface InventoryItemRepository extends CrudRepository<InventoryItemDetails, Integer> {
}
