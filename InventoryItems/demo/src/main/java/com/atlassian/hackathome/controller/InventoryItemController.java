package com.atlassian.hackathome.controller;


import java.util.List;

import com.atlassian.hackathome.model.InventoryItemDetails;
import com.atlassian.hackathome.service.InventoryItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InventoryItemController {

  @Autowired
  InventoryItemService inventoryItemService;

  @GetMapping("/listInventoryItems")
  private List listInventoryItems() {
    return inventoryItemService.listInventoryItems();
  }

  @PostMapping("/saveInventoryItem")
  private int saveInventoryItem(@RequestBody InventoryItemDetails inventoryItemDetails) {
    inventoryItemService.saveInventoryItem(inventoryItemDetails);
    inventoryItemDetails.getDescription().length();
    return inventoryItemDetails.getId();
  }
}
