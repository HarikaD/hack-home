package com.atlassian.hackathome.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class InventoryItemDetails {

  @Id
  @GeneratedValue
  private int id;

  private String inventoryCategory;

  private Date dateOfInwarding;

  private String description;

  private Integer quantity;

  private String supplierName;

  public int getId() {
    return id;
  }

  public String getInventoryCategory() {
    return inventoryCategory;
  }

  public Date getDateOfInwarding() {
    return dateOfInwarding;
  }

  public String getDescription() {
    return description;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public String getSupplierName() {
    return supplierName;
  }

}
