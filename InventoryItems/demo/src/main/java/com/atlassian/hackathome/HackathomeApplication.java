package com.atlassian.hackathome;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HackathomeApplication {

	public static void main(String[] args) {
		SpringApplication.run(HackathomeApplication.class, args);
	}

}
