package com.atlassian.hackathome.service;

import com.atlassian.hackathome.model.InventoryItemDetails;
import com.atlassian.hackathome.repository.InventoryItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
@Service
public class InventoryItemService {

  @Autowired
  InventoryItemRepository inventoryItemRepository;


  public List listInventoryItems() {
    List inventoryItems = new ArrayList();
    inventoryItemRepository.findAll().forEach(inventoryItem -> inventoryItems.add(inventoryItem));
    return inventoryItems;
  }
  public void saveInventoryItem(InventoryItemDetails inventoryItemDetails) {
    inventoryItemRepository.save(inventoryItemDetails);
  }
}
