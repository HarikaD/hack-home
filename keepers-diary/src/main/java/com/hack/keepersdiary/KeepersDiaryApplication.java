package com.hack.keepersdiary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KeepersDiaryApplication {

	public static void main(String[] args) {
		SpringApplication.run(KeepersDiaryApplication.class, args);
	}

}
