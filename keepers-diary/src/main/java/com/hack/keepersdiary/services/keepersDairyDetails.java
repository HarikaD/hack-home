package com.hack.keepersdiary.services;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@SpringBootApplication
public class keepersDairyDetails {

    @RequestMapping(value = "/keepers-diary")
    public String available(@RequestParam(value="enc_name") String enc_name,@RequestParam(value="ani_name") String ani_name,@RequestParam (value="obs") String obs,@RequestParam (value="label") List<String> label) {
        return "Record added with Enclosure name : "+enc_name+" Animal name : "+ani_name+" Animal Observation : "+obs+" and labels : "+label;
    }
}
